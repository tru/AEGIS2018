mkdir -p /home/aegis2018/.ssh

echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDJENlrogoggL3vZPd0V8+Ym41Id9xrRaA+17UPv9q+J+RgXIZhdrAzMP5ikOwUvMWwf5MqXcFgvo+HmEEoHW+VGVufnmYxGEQi+BWuQpgxONJBdMo3RgzWIwpF2QKt6wd4zqR7T56RvTXxd/GKIQXMsr56qVsrNhxiaLMrDBV8zUW1J1C4n5tlyKItalBaFI21C69dGtxM6KylyMJnbx/bV8gE4sYm3+u9gRp9SMdXqGffphoHi/urBwkXGiedA4fnGylthJNwHvXdN9lLMMPEzqkiSOebSXNflRL1r70UZ9NMPPU2Mpqz5uEyRNoq2JHqRtyNNCZftgL1R7Ge33Gh aegis2018'> /home/aegis2018/.ssh/authorized_keys
chmod 0700 /home/aegis2018/.ssh
chmod 0600 /home/aegis2018/.ssh/authorized_keys
chown -R aegis2018:aegis2018 /home/aegis2018/.ssh
restorecon -rv /home/aegis2018

mkdir -p /home/centos/.ssh
echo ' ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCh4PgY+2MjCCCvH+IRfqpJnVKk5eqKE+mGH93S6nn8pHoVHmfHsafz5VCfFgJ6oDEPbEHqJAfcY+l4A5bAFSB8PA95X63JdT9SkcOTvjaXaM0NPiXblBwhmxe8F0Plp7/LyjNKHLdCful24Urp2gStl14xODczcUTtjVNC1s3GiTIm+CnEaVuC7rGMAgK305/TrLdpAzztZmubdm4FjCQs7JSSA7zr5v1RjzLQbN7+2KubCSFXBXWFt9lFeC59iWCi0P1XCNd0TGdTUKal2f8nM58ra36M64EBWnklAy2KCP2Fj+Fuoo3oEZLPqVoiVyTDQnCfr3TShRkVuAYLqD0L centos' > /home/centos/.ssh/authorized_keys
chmod 0700 /home/centos/.ssh
chmod 0600 /home/centos/.ssh/authorized_keys
chown -R centos:centos /home/centos/.ssh
restorecon -rv /home/centos
echo 'centos ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
restorecon -rv /etc/sudoers
